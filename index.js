const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 4004;
app.use(express.json());
app.use(express.urlencoded({extended : true}));

mongoose.connect("mongodb+srv://guardoej18:darabz312@zuitt-bootcamp.d2vle2v.mongodb.net/s36-activity",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("We're now connected to the cloud database."));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Server is now running at localhost: ${port}`));
const Task = require("../models/task");

// Get tasks by ID
module.exports.getTaskById = (taskId) => {
	
	return Task.findById(taskId).then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// Create a task
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// Update a task
module.exports.updateTask = (taskId, updatedTask) => {
	
	return Task.findById(taskId).then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} 
		task.name = updatedTask.name;

		return task.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}